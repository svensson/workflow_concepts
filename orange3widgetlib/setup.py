from esrf2orange3.setuptools import setup

if __name__ == "__main__":
    setup(
        __file__,
        name="orange3widgetlib",
        version="0.0.1a",
        description="Add-on containing example widgets",
    )
