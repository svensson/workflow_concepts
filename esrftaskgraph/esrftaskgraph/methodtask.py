from esrftaskgraph.task import Task
from esrftaskgraph.utils import import_method


class MethodExecutorTask(Task):
    INPUT_NAMES = Task.INPUT_NAMES + ["method"]
    OUTPUT_NAMES = Task.OUTPUT_NAMES + ["return_value"]

    def _parse_method_kwargs(self, kwargs):
        kwargs.pop("method")

    def process(self):
        fullname = self.input.method.value
        method = import_method(fullname)
        kwargs = self.input_values
        self._parse_method_kwargs(kwargs)
        result = method(**kwargs)
        self.output.return_value.value = result


class PpfMethodExecutorTask(MethodExecutorTask):
    OPTIONAL_INPUT_NAMES = MethodExecutorTask.OPTIONAL_INPUT_NAMES + ["return_value"]

    def _parse_method_kwargs(self, kwargs):
        super()._parse_method_kwargs(kwargs)
        return_value = kwargs.pop("return_value", None)
        if return_value:
            kwargs.update(return_value)

    def process(self):
        fullname = self.input.method.value
        method = import_method(fullname)
        kwargs = self.input_values
        self._parse_method_kwargs(kwargs)
        result = method(**kwargs)
        return_value = dict(kwargs)
        return_value.update(result)
        self.output.return_value.value = return_value
