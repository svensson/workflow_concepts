import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import taskgraphs
from taskgraphlib import check_pipeline
from taskgraphlib import check_pipeline_output

# Logging makes multiprocessing hangs?
# https://pythonspeed.com/articles/python-multiprocessing/

# logging.getLogger("pypushflow").setLevel(logging.DEBUG)
# logging.getLogger("pypushflow").addHandler(logging.StreamHandler(sys.stdout))
logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def test_acyclic_job1(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph1()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)


def test_acyclic_job2(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.acyclic_graph2()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)


def test_cyclic_job1(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.cyclic_graph1()
    result = job(graph, varinfo=varinfo)
    check_pipeline_output(result, expected, varinfo)


def test_cyclic_job2(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = taskgraphs.cyclic_graph2()
    result = job(graph, varinfo=varinfo)
    check_pipeline_output(result, expected, varinfo)
