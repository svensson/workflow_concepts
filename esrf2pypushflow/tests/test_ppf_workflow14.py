import sys
import logging
from esrf2pypushflow import job
from taskgraphlib import check_pipeline

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def submodel14a():
    nodes = [
        {
            "id": "addtask2aa",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {
            "id": "addtask2ab",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {"id": "In", "ppfport": "input"},
        {"id": "Out", "ppfport": "output"},
    ]

    links = [
        {"source": "In", "target": "addtask2aa", "all_arguments": True},
        {"source": "addtask2aa", "target": "addtask2ab", "all_arguments": True},
        {"source": "addtask2ab", "target": "Out", "all_arguments": True},
    ]

    graph = {
        "directed": True,
        "graph": {"name": "submodel14a"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph


def submodel14b():
    nodes = [
        {"id": "submodel14a", "graph": submodel14a()},
        {"id": "In", "ppfport": "input"},
        {"id": "Out", "ppfport": "output"},
    ]

    links = [
        {
            "source": "In",
            "target": "submodel14a",
            "links": [
                {
                    "source": "In",
                    "target": "In",
                    "all_arguments": True,
                }
            ],
        },
        {
            "source": "submodel14a",
            "target": "Out",
            "links": [
                {
                    "source": "Out",
                    "target": "Out",
                    "all_arguments": True,
                }
            ],
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "submodel14b"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph


def workflow14():
    nodes = [
        {
            "id": "addtask1",
            "inputs": {"value": 1},
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {
            "id": "addtask3",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {"id": "submodel14b", "graph": submodel14b()},
    ]

    links = [
        {
            "source": "addtask1",
            "target": "submodel14b",
            "links": [
                {
                    "source": "addtask1",
                    "target": "in14b",
                    "all_arguments": True,
                }
            ],
        },
        {
            "source": "submodel14b",
            "target": "addtask3",
            "links": [
                {
                    "source": "out14b",
                    "target": "addtask3",
                    "all_arguments": True,
                }
            ],
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "workflow14"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    expected_results = {
        "addtask1": {"return_value": {"value": 2}},
        ("submodel14b", "in14b"): {"return_value": {"value": 2}},
        ("submodel14b", "addtask2ba"): {"return_value": {"value": 3}},
        ("submodel14b", ("submodel14a", "in14a")): {"return_value": {"value": 3}},
        ("submodel14b", ("submodel14a", "addtask2aa")): {"return_value": {"value": 4}},
        ("submodel14b", ("submodel14a", "addtask2ab")): {"return_value": {"value": 5}},
        ("submodel14b", ("submodel14a", "out14a")): {"return_value": {"value": 5}},
        ("submodel14b", "addtask2bb"): {"return_value": {"value": 6}},
        ("submodel14b", "out14b"): {"return_value": {"value": 6}},
        "addtask3": {"return_value": {"value": 7}},
    }

    return graph, expected_results


def test_workflow14(tmpdir):
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow14()
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
