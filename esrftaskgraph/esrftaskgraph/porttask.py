from esrftaskgraph.task import Task


class PpfPortTask(Task):
    OUTPUT_NAMES = Task.OUTPUT_NAMES + ["return_value"]
    OPTIONAL_INPUT_NAMES = Task.OPTIONAL_INPUT_NAMES + ["return_value"]

    def process(self):
        passthroughdata = self.input.return_value.value
        if passthroughdata is self.MISSING_DATA:
            self.output.return_value.value = dict()
        else:
            self.output.return_value.value = dict(passthroughdata)
