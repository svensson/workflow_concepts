import sys
import logging
import pytest
from esrf2pypushflow import job
from taskgraphlib import check_pipeline

logging.getLogger("esrf2pypushflow").setLevel(logging.DEBUG)
logging.getLogger("esrf2pypushflow").addHandler(logging.StreamHandler(sys.stdout))


def submodel12():
    nodes = [
        {
            "id": "addtask2a",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {
            "id": "addtask2b",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {"id": "in", "ppfport": "input"},
        {"id": "out", "ppfport": "output"},
    ]

    links = [
        {"source": "in", "target": "addtask2a", "all_arguments": True},
        {"source": "addtask2a", "target": "addtask2b", "all_arguments": True},
        {"source": "addtask2b", "target": "out", "all_arguments": True},
    ]

    graph = {
        "directed": True,
        "graph": {"name": "submodel12"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    return graph


def workflow12(startvalue, withsubmodel_startvalue):
    nodes = [
        {
            "id": "addtask1",
            "inputs": {"value": startvalue},
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {
            "id": "addtask2",
            "ppfmethod": "tests.test_ppf_actors.pythonActorAdd.run",
        },
        {"id": "submodel12", "graph": submodel12()},
    ]

    links = [
        {
            "source": "addtask1",
            "target": "submodel12",
            "links": [
                {
                    "source": "addtask1",
                    "target": "in",
                    "all_arguments": True,
                    "conditions": {"value": withsubmodel_startvalue + 1},
                }
            ],
        },
        {
            "source": "submodel12",
            "target": "addtask2",
            "links": [
                {
                    "source": "out",
                    "target": "addtask2",
                    "all_arguments": True,
                }
            ],
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "workflow12"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    value = startvalue
    value += 1
    expected_results = {"addtask1": {"return_value": {"value": value}}}
    if startvalue == withsubmodel_startvalue:
        expected_results[("submodel12", "in")] = {"return_value": {"value": value}}
        value += 1
        expected_results[("submodel12", "addtask2a")] = {
            "return_value": {"value": value}
        }
        value += 1
        expected_results[("submodel12", "addtask2b")] = {
            "return_value": {"value": value}
        }
        expected_results[("submodel12", "out")] = {"return_value": {"value": value}}
        value += 1
        expected_results["addtask2"] = {"return_value": {"value": value}}

    return graph, expected_results


@pytest.mark.parametrize("startvalue", [0, 1])
def test_workflow12(startvalue, tmpdir):
    withsubmodel_startvalue = 1
    varinfo = {"root_uri": str(tmpdir)}
    graph, expected = workflow12(startvalue, withsubmodel_startvalue)
    job(graph, varinfo=varinfo)
    check_pipeline(graph, expected, varinfo)
