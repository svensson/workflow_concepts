orangewidgetlib
===============

*Orange3* widgets for *tasklib* tasks.

Equivalent projects are est.orangecontrib + est.est.gui, tomwer.orangecontrib + tomwer.tomwer.gui , darfix.orangecontrib + darfix.darfix.gui, ...

Categories:
-----------

* evaluate (existing Orange3 category)
    * Adder1
    * Adder2
* examplecategory1
    * Adder1
    * Adder2

All four OWWidgets execute a `tasklib.SumTask` task using the Orange3 GUI.
