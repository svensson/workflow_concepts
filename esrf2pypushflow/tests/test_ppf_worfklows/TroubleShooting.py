from tests.test_ppf_worfklows.CommonPrepareExperiment import CommonPrepareExperiment


def TroubleShooting(inData):

    nodes = [
        {
            "id": "Prepare Trouble Shooting",
            "inputs": inData,
            "ppfmethod": "mx.src.prepareTroubleShooting.run",
        },
        {"id": "Common Prepare Experiment", "graph": CommonPrepareExperiment()},
        {
            "id": "Check move of phi",
            "ppfmethod": "mx.src.checkMoveOfPhi.run",
        },
        {
            "id": "All tests ok",
            "ppfmethod": "mx.src.troubleShootingAllTestsOk.run",
        },
        {
            "id": "Set ISPyB to success",
            "ppfmethod": "mx.src.ispyb_set_status_success.run",
        },
        {
            "id": "Set ISPyB status to success with error message",
            "ppfmethod": "mx.src.ispyb_set_status_success_with_errors.run",
        },
    ]

    links = [
        {
            "source": "Prepare Trouble Shooting",
            "target": "Common Prepare Experiment",
            "links": [
                {
                    "source": "Prepare Trouble Shooting",
                    "target": "In",
                    "all_arguments": True,
                }
            ],
        },
        {
            "source": "Common Prepare Experiment",
            "target": "Check move of phi",
            "links": [
                {"source": "Out", "target": "Check move of phi", "all_arguments": True}
            ],
        },
        {
            "source": "Check move of phi",
            "target": "All tests ok",
            "conditions": {"flagPhiMoved": "true"},
            "all_arguments": True,
        },
        {
            "source": "Check move of phi",
            "target": "Set ISPyB status to success with error message",
            "conditions": {"flagPhiMoved": "false"},
            "all_arguments": True,
        },
        {
            "source": "All tests ok",
            "target": "Set ISPyB to success",
            "all_arguments": True,
        },
    ]

    graph = {
        "directed": True,
        "graph": {"name": "Trouble Shooting"},
        "links": links,
        "multigraph": False,
        "nodes": nodes,
    }

    expected_results = {}

    return graph, expected_results
