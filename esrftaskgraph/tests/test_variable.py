import pytest
from esrftaskgraph.variable import Variable
from esrftaskgraph.variable import VariableContainer

VALUES = [None, True, 10, "string", 10.1, [1, 2, 3], {"1": 1, "2": {"2": [10, 20]}}]


def test_variable_missing_data(tmpdir):
    v = Variable(root_uri=str(tmpdir))
    assert not v.available
    assert not v.exists
    assert not v.value
    assert not v.uri
    v.dump()
    v.load()
    assert not v.available
    assert not v.exists
    assert not v.value
    assert not v.uri


def test_variable_uhash(tmpdir):
    for value in VALUES:
        v1 = Variable(value, root_uri=str(tmpdir))
        v2 = Variable(value, root_uri=str(tmpdir))
        v3 = Variable(uhash=v1, root_uri=str(tmpdir))
        v4 = Variable(uhash=v1.uhash, root_uri=str(tmpdir))
        assert v1.uhash == v2.uhash
        assert v1.uhash == v3.uhash
        assert v1.uhash == v4.uhash
        v1.value = 99999
        assert v1.uhash != v2.uhash
        assert v1.uhash == v3.uhash
        assert v1.uhash != v4.uhash


def test_variable_nonce(tmpdir):
    v1 = Variable(9999, root_uri=str(tmpdir))
    v2 = Variable(value=9999, uhash_nonce=1, root_uri=str(tmpdir))
    assert v1.uhash != v2.uhash
    assert v1 != v2
    assert v1.value == v2.value
    v2 = Variable(uhash=v1, uhash_nonce=1, root_uri=str(tmpdir))
    assert v1.uhash != v2.uhash
    assert v1 != v2
    assert v1.value != v2.value
    v2 = Variable(uhash=v1.uhash, uhash_nonce=1, root_uri=str(tmpdir))
    assert v1.uhash != v2.uhash
    assert v1 != v2
    assert v1.value != v2.value


def test_variable_compare(tmpdir):
    for value in VALUES:
        v1 = Variable(value, root_uri=str(tmpdir))
        v2 = Variable(value, root_uri=str(tmpdir))
        assert v1 == v2
        assert v1 == value
        assert v2 == value
        v1.value = 99999
        assert v1 != v2
        assert v1 != value
        assert v2 == value


def test_variable_uri(tmpdir):
    for value in VALUES:
        v1 = Variable(value, root_uri=str(tmpdir))
        v2 = Variable(value, root_uri=str(tmpdir))
        assert v1.uri is not None
        assert v1.uri == v2.uri
        v1.value = 99999
        assert v1.uri is not None
        assert v1.uri != v2.uri


def test_variable_chain(tmpdir):
    v1 = Variable(9999, root_uri=str(tmpdir))
    v2 = Variable(uhash=v1, root_uri=str(tmpdir))
    assert v1 == v1
    v1.value += 1
    assert v1 == v2
    v1.dump()
    assert v1 == v2
    assert v1.value == v2.value
    v1.value += 1
    assert v1 == v2
    assert v1.value != v2.value
    with pytest.raises(RuntimeError):
        v2.validate()


def test_variable_persistency(tmpdir):
    for value in VALUES:
        v1 = Variable(value, root_uri=str(tmpdir))
        v2 = Variable(value, root_uri=str(tmpdir))
        v3 = Variable(uhash=v1.uhash, root_uri=str(tmpdir))
        v4 = Variable(uhash=v2, root_uri=str(tmpdir))

        assert not v1.exists
        assert not v2.exists
        assert not v3.exists
        assert not v4.exists
        assert v1.available
        assert v2.available
        assert not v3.available
        assert not v4.available

        v1.dump()
        assert v1.exists
        assert v2.exists
        assert v3.exists
        assert v4.exists
        assert v1.available
        assert v2.available
        assert v3.available
        assert v4.available
        v1.validate()
        v2.validate()
        v3.validate()
        v4.validate()


def test_variable_container_uhash(tmpdir):
    values = {f"var{i}": value for i, value in enumerate(VALUES, 1)}
    v1 = VariableContainer(value=values, root_uri=str(tmpdir))
    v2 = VariableContainer(value=v1, root_uri=str(tmpdir))
    v3 = VariableContainer(uhash=v1, root_uri=str(tmpdir))
    v4 = VariableContainer(uhash=v1.uhash, root_uri=str(tmpdir))

    v1[next(iter(v1))].value = 9999
    assert v1.uhash == v2.uhash
    assert v1.uhash == v3.uhash
    assert v1.uhash != v4.uhash


def test_variable_container_compare(tmpdir):
    values = {f"var{i}": value for i, value in enumerate(VALUES, 1)}
    v1 = VariableContainer(value=values, root_uri=str(tmpdir))
    v2 = VariableContainer(value=v1, root_uri=str(tmpdir))
    v3 = VariableContainer(uhash=v1, root_uri=str(tmpdir))
    v4 = VariableContainer(uhash=v1.uhash, root_uri=str(tmpdir))

    v1.dump()
    v1[next(iter(v1))].value = 9999
    assert v1 == v2
    assert v1 != v3
    assert v1 != v4
    nfiles = len(values) + 1
    assert len(tmpdir.listdir()) == nfiles

    v1.dump()
    assert v1 == v2
    assert v1 == v3
    assert v1 != v4
    assert len(tmpdir.listdir()) == nfiles + 2


def test_variable_container_persistency(tmpdir):
    values = {f"var{i}": value for i, value in enumerate(VALUES, 1)}
    v1 = VariableContainer(value=values, root_uri=str(tmpdir))
    v2 = VariableContainer(value=v1, root_uri=str(tmpdir))
    v3 = VariableContainer(uhash=v1, root_uri=str(tmpdir))
    v4 = VariableContainer(uhash=v1.uhash, root_uri=str(tmpdir))

    assert v1.keys() == v2.keys()
    for v in v1.values():
        assert v.uhash != v1.uhash
    for k in v1:
        assert v1[k] is v2[k]
    assert v1.available
    assert v2.available
    assert not v3.available
    assert not v1.exists
    assert not v2.exists
    assert not v3.exists
    assert not v4.exists
    assert len(v1) == len(values)
    assert len(v2) == len(values)
    assert len(v3) == 0
    assert len(v4) == 0
    assert v1 == v2
    assert v2 != v3
    assert v2 != v4
    assert len(tmpdir.listdir()) == 0

    v1.dump()
    assert len(tmpdir.listdir()) == len(values) + 1
    assert v1.available
    assert v2.available
    assert v3.available
    assert v4.available
    assert v1.exists
    assert v2.exists
    assert v3.exists
    assert v4.exists
    assert len(v1) == len(values)
    assert len(v2) == len(values)
    assert len(v3) == len(values)
    assert len(v4) == len(values)
    for k in v1:
        assert v1[k] is not v3[k]
    assert v1 == v2 == v3 == v4
    assert len(tmpdir.listdir()) == len(values) + 1
