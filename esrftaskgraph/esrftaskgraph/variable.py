import os
import string
import random
import json
from collections.abc import Mapping, MutableMapping
from contextlib import contextmanager
from esrftaskgraph import hashing


class PersistencyError(RuntimeError):
    pass


class UriNotFoundError(PersistencyError):
    pass


def random_string(n):
    return "".join(random.choices(string.ascii_letters + string.digits, k=n))


def nonexisting_tmp_file(filename):
    tmpname = filename + ".tmp" + random_string(6)
    while os.path.exists(tmpname):
        tmpname = filename + ".tmp" + random_string(6)
    return tmpname


@contextmanager
def atomic_write(filename):
    tmpname = nonexisting_tmp_file(filename)
    dirname = os.path.dirname(tmpname)
    try:
        if dirname:
            os.makedirs(dirname, exist_ok=True)
        with open(tmpname, mode="w") as f:
            yield f
    except Exception:
        try:
            os.unlink(tmpname)
        except FileNotFoundError:
            pass
        raise
    os.rename(tmpname, filename)


class Variable(hashing.UniversalHashable):
    """Has a runtime representation (python object) and a persistent
    representation (JSON).

    TODO: make abstraction of persistent representation
    """

    def __init__(
        self,
        value=hashing.UniversalHashable.MISSING_DATA,
        root_uri=None,
        **kw,
    ):
        """
        :param value: the runtime representation
        :param root_uri: for the persistent representation
        :param **kw: see `UniversalHashable`
        """
        self._disable_persistency = not root_uri
        super().__init__(**kw)
        self._root_uri = root_uri
        self._runtime_representation = self.MISSING_DATA
        self.value = value

    def _uhash_data(self):
        if self._disable_persistency:
            super()._uhash_data()
        else:
            return self._runtime_representation

    def __repr__(self):
        return f"{type(self).__qualname__}({self.uri})"

    def __eq__(self, other):
        if isinstance(other, hashing.UniversalHashable):
            return super().__eq__(other)
        else:
            return self.value == other

    @property
    def value(self):
        if self._runtime_representation is self.MISSING_DATA:
            self.value = self.load(raise_error=False)
        return self._runtime_representation

    @value.setter
    def value(self, v):
        self._runtime_representation = v

    @property
    def uri(self):
        """uri of the persistent representation"""
        uhash = self.uhash
        if uhash is None:
            return
        filename = f"{uhash}.json"
        if self._root_uri:
            filename = os.path.join(self._root_uri, filename)
        return filename

    def dump(self):
        """From runtime to persistent representation (never overwrite).
        Creating the persistent representation needs to be atomic.

        This silently returns when:
        - persistency is disabled
        - already persisted
        - data is not available (MISSING_DATA)
        - non value URI can be constructed
        """
        if self._disable_persistency or self.exists or not self.available:
            return
        filename = self.uri
        if not filename:
            return
        data = self.value
        with atomic_write(filename) as f:
            json.dump(self.serialize(data), f)

    def load(self, raise_error=True):
        """From persistent to runtime representation

        This silently returns when:
        - persistency is disabled
        """
        if self._disable_persistency:
            return self._runtime_representation
        filename = self.uri
        if not filename:
            return self._runtime_representation
        try:
            with open(filename, mode="r") as f:
                return self.deserialize(json.load(f))
        except FileNotFoundError as e:
            if raise_error:
                raise UriNotFoundError(filename) from e
            else:
                return self._runtime_representation
        except Exception as e:
            if raise_error:
                raise PersistencyError(filename) from e
            else:
                return self._runtime_representation

    def serialize(self, value):
        """Before runtime to persistent"""
        return value

    def deserialize(self, value):
        """Before persistent to runtime"""
        return value

    @property
    def exists(self):
        """Has a persistent representation"""
        return self._exists()

    @property
    def available(self):
        """Has a runtime representation"""
        try:
            return self._available()
        except PersistencyError:
            # Lazy loading failed
            return False

    def _exists(self):
        """Has a persistent representation"""
        filename = self.uri
        if filename:
            return os.path.isfile(filename)
        else:
            return False

    def _available(self):
        """Has a runtime representation"""
        return self.value is not self.MISSING_DATA

    def validate(self):
        if not self.exists:
            raise RuntimeError("Has no persistent representation")
        if not self.available:
            raise RuntimeError("Has no runtime representation")
        return self.value == self.load()

    def force_non_existing(self):
        while self.exists:
            super()._uhash_randomize()


class VariableContainer(MutableMapping, Variable):
    def __init__(self, value=hashing.UniversalHashable.MISSING_DATA, **varparams):
        self.__varparams = varparams
        super().__init__(value=hashing.UniversalHashable.MISSING_DATA, **varparams)
        if value:
            self.update(value)

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __getitem__(self, name):
        return self.value[name]

    def __setitem__(self, name, value):
        if isinstance(value, Variable):
            var = value
        else:
            varparams = dict(self.__varparams)
            if isinstance(value, hashing.UniversalHash):
                varparams["uhash"] = value
                varparams["uhash_nonce"] = None
            else:
                varparams["value"] = value
                uhash_nonce = varparams.pop("uhash_nonce", None)
                varparams["uhash_nonce"] = uhash_nonce, name
            var = Variable(**varparams)
        if not self.container_available and not self.container_exists:
            self.value = dict()
        self.value[name] = var

    def __delitem__(self, name):
        adict = self.value
        if isinstance(adict, dict):
            del self.value[name]

    def __iter__(self):
        adict = self.value
        if isinstance(adict, dict):
            return iter(adict)
        else:
            return iter(tuple())

    def __len__(self):
        adict = self.value
        if isinstance(adict, dict):
            return len(adict)
        else:
            return 0

    def serialize(self, value):
        return {k: str(v.uhash) for k, v in self.items()}

    def deserialize(self, value):
        adict = dict()
        varparams = dict(self.__varparams)
        varparams["uhash_nonce"] = None
        for k, v in value.items():
            varparams["uhash"] = hashing.UniversalHash(v)
            adict[k] = Variable(**varparams)
        return adict

    def dump(self):
        for v in self.values():
            v.dump()
        super().dump()

    @property
    def container_exists(self):
        return super()._exists()

    def _exists(self):
        if self.container_exists:
            return all(v.exists for v in self.values())
        else:
            return False

    @property
    def container_available(self):
        try:
            return super()._available()
        except PersistencyError:
            # Lazy loading failed
            return False

    def _available(self):
        if self.container_available:
            return all(v.available for v in self.values())
        else:
            return False

    def force_non_existing(self):
        super().force_non_existing()
        for v in self.values():
            v.force_non_existing()

    @property
    def variable_uhashes(self):
        return self.serialize(self.value)

    @property
    def variable_values(self):
        return {k: v.value for k, v in self.items()}

    def update_values(self, items):
        if isinstance(items, Mapping):
            items = items.items()
        for k, v in items:
            self[k].value = v
