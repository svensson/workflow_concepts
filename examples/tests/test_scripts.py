import os
import sys
from glob import glob
import subprocess
import pytest

ROOT_DIR = os.path.dirname(os.path.dirname(__file__))


def test_scripts(tmpdir):
    scripts = glob(os.path.join(ROOT_DIR, "*.py"))
    for script in scripts:
        args = [sys.executable, script, "--root_uri", str(tmpdir)]
        subprocess.run(args).check_returncode()


def test_running_pipelines(tmpdir):
    script = os.path.join(ROOT_DIR, "running_pipelines.py")
    args = [sys.executable, script, "--root_uri", str(tmpdir)]
    with pytest.raises(subprocess.CalledProcessError):
        subprocess.run(args + ["--scheduler", "none"]).check_returncode()
    subprocess.run(args + ["--scheduler", "multiprocess"]).check_returncode()
    subprocess.run(args + ["--scheduler", "none"]).check_returncode()
