from esrftaskgraph import load_graph
from esrftaskgraph import Variable


def check_pipeline(taskgraph, expected, varinfo):
    tasks = dict()
    taskgraph = load_graph(taskgraph)
    assert not taskgraph.is_cyclic, "Can only check DAGs"
    for node in taskgraph.graph.nodes:
        task = taskgraph.instantiate_task_static(node, tasks=tasks, varinfo=varinfo)
        value = expected.get(node)
        if value is None:
            assert not task.done, node
        else:
            assert task.done, node
            try:
                assert task.output.value == value, node
            except AssertionError:
                raise
            except Exception as e:
                raise RuntimeError(f"{node} does not have a result") from e


def check_pipeline_output(result, expected, varinfo):
    for k, v in expected.items():
        uhash = result[k]
        var = Variable(uhash=uhash, **varinfo)
        assert var.value == v
