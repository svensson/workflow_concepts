import time


def run(c=None, **kwargs):

    time.sleep(1)
    if c is None:
        raise RuntimeError("Missing argument 'c'!")
    d = c + 1
    return {"d": d}
