from esrftaskgraph.task import Task
from esrftaskgraph.methodtask import MethodExecutorTask
from esrftaskgraph.methodtask import PpfMethodExecutorTask
from esrftaskgraph.scripttask import ScriptExecutorTask
from esrftaskgraph.porttask import PpfPortTask
from esrftaskgraph.utils import import_method
from esrftaskgraph.utils import import_qualname

TASK_EXECUTABLE_ATTRIBUTE = (
    "class",
    "method",
    "ppfmethod",
    "ppfport",
    "script",
    "graph",
)
TASK_EXECUTABLE_ATTRIBUTE_STR = (
    ", ".join(map(repr, TASK_EXECUTABLE_ATTRIBUTE[:-1]))
    + " or "
    + repr(TASK_EXECUTABLE_ATTRIBUTE[-1])
)
TASK_EXECUTABLE_ERROR_MSG = f"Task requires the {TASK_EXECUTABLE_ATTRIBUTE_STR} key"


def instantiate_task(node_attrs, varinfo=None, inputs=None):
    """
    :param dict node_attrs: node attributes of the graph representation
    :param dict varinfo: `Variable` constructor arguments
    :param dict or None inputs: dynamic inputs (from other tasks)
    :returns Task:
    """
    # Static inputs
    kwargs = dict(node_attrs.get("inputs", dict()))
    # Dynamic inputs (from other tasks)
    if inputs:
        kwargs.update(inputs)
    # `Variable` arguments
    kwargs["varinfo"] = varinfo

    # Instantiate task
    task_class = node_attrs.get("class")
    if task_class:
        return Task.instantiate(task_class, **kwargs)
    method = node_attrs.get("method")
    if method:
        return MethodExecutorTask(method=method, **kwargs)
    method = node_attrs.get("ppfmethod")
    if method:
        return PpfMethodExecutorTask(method=method, **kwargs)
    script = node_attrs.get("script")
    if script:
        return ScriptExecutorTask(script=script, **kwargs)
    ppfport = node_attrs.get("ppfport")
    if ppfport:
        return PpfPortTask(ppfport=ppfport, **kwargs)
    raise ValueError(TASK_EXECUTABLE_ERROR_MSG)


def task_executable(node_attrs):
    task_class = node_attrs.get("class")
    if task_class:
        return task_class, import_qualname
    method = node_attrs.get("method")
    if method:
        return method, import_method
    method = node_attrs.get("ppfmethod")
    if method:
        return method, import_method
    script = node_attrs.get("script")
    if script:
        return script, None
    ppfport = node_attrs.get("ppfport")
    if ppfport:
        return ppfport, None
    raise ValueError(TASK_EXECUTABLE_ERROR_MSG)


def get_task_class(node_attrs):
    task_class = node_attrs.get("class")
    if task_class:
        return Task.get_subclass(task_class)
    method = node_attrs.get("method")
    if method:
        return MethodExecutorTask
    method = node_attrs.get("ppfmethod")
    if method:
        return PpfMethodExecutorTask
    script = node_attrs.get("script")
    if script:
        return ScriptExecutorTask
    ppfport = node_attrs.get("ppfport")
    if ppfport:
        return PpfPortTask
    raise ValueError(TASK_EXECUTABLE_ERROR_MSG)
