import importlib


def qualname(_type):
    mod_name = _type.__module__
    if not mod_name or mod_name == str.__module__:
        return _type.__name__  # Avoid reporting builtins
    else:
        return mod_name + "." + _type.__name__


def import_qualname(qualname):
    if not isinstance(qualname, str):
        raise TypeError(qualname, type(qualname))
    module_name, dot, name = qualname.rpartition(".")
    if not module_name:
        raise ImportError(f"cannot import {qualname}")
    module = importlib.import_module(module_name)
    try:
        return getattr(module, name)
    except AttributeError:
        raise ImportError(f"cannot import {name} from {module_name}")


def import_method(qualname):
    method = import_qualname(qualname)
    if not callable(method):
        raise RuntimeError(repr(qualname) + " is not callable")
    return method
