from esrftaskgraph import utils


class Registered(type):
    def __new__(metacls, name, bases, attr, register=True):
        """Register class when defined"""
        cls = super().__new__(metacls, name, bases, attr)
        if not hasattr(cls, "_SUBCLASS_REGISTRY"):
            cls._SUBCLASS_REGISTRY = {}
            cls.get_subclass_names = classmethod(get_subclass_names)
            cls.get_subclass = classmethod(get_subclass)
            cls.get_subclasses = classmethod(get_subclasses)
            cls.registry_name = classmethod(registry_name)

        if not register:
            return cls

        # Register the subclass
        reg_name = registry_name(cls)
        ecls = cls._SUBCLASS_REGISTRY.get(reg_name)
        if ecls is not None:
            raise NotImplementedError(
                f"Registry name {reg_name} is already taken by {repr(ecls)}"
            )
        cls._SUBCLASS_REGISTRY[reg_name] = cls
        return cls


def registry_name(cls):
    return utils.qualname(cls)


def get_subclass(cls, reg_name, _retry=False):
    """Retrieving a derived class"""
    _class = cls._SUBCLASS_REGISTRY.get(reg_name)
    if _class is None:
        candidates = []
        for name, value in cls._SUBCLASS_REGISTRY.items():
            if name.endswith("." + reg_name):
                candidates.append(name)
        if len(candidates) == 1:
            _class = cls._SUBCLASS_REGISTRY.get(candidates[0])
    if _class is None:
        if _retry:
            lst = cls.get_subclass_names()
            raise RuntimeError(
                f"Class {repr(reg_name)} is not imported. Imported classes are {repr(lst)}"
            )
        else:
            utils.import_qualname(reg_name)
            _class = cls.get_subclass(reg_name, _retry=True)
    return _class


def get_subclass_names(cls):
    return list(cls._SUBCLASS_REGISTRY.keys())


def get_subclasses(cls):
    return list(cls._SUBCLASS_REGISTRY.values())
