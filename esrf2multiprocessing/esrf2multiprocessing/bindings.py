import os
import logging
import traceback
import multiprocessing_dag as mpdag
from esrftaskgraph import load_graph


logger = logging.getLogger(__name__)

# Runner: task scheduler
# Pipeline: task graph instance
# Task: task
# BaseCommand: executable (no output)


class TaskCommand(mpdag.BaseCommand):
    def __init__(self, taskgraph, node, uri, varinfo):
        self.taskgraph = taskgraph
        self.graph = self.taskgraph.graph
        self.node = node
        self.uri = uri
        if varinfo is None:
            varinfo = dict()
        self.varinfo = varinfo
        self.prefix = f"[pid:{os.getpid()}] [{node}] [{uri}] "
        super().__init__()

    def run(self):
        try:
            self.runtask()
        except Exception:
            traceback.print_exc()
            raise
        return True

    def runtask(self):
        logger.info(self.prefix + "instantiate tasks ...")
        task = self.taskgraph.instantiate_task_static(self.node, varinfo=self.varinfo)
        logger.info(self.prefix + "run ...")
        task.run()
        logger.info(self.prefix + f"output = {task.output.value}")


def convert_graph(taskgraph, varinfo):
    pipeline = mpdag.Pipeline(str(taskgraph))
    for node in taskgraph.graph.nodes:
        task = mpdag.Task(node)
        uri = task.uri()
        cmd = TaskCommand(taskgraph, node, uri, varinfo)
        task.add_command(cmd)
        pipeline.add(task)
    for node in taskgraph.graph.nodes:
        task = pipeline.nodes[node]
        for upstream in taskgraph.predecessors(node):
            pipeline.add_dependency(pipeline.nodes[upstream], task)
    return pipeline


def job(graph, representation=None, varinfo=None):
    esrfgraph = load_graph(source=graph, representation=representation)
    if esrfgraph.is_cyclic:
        raise RuntimeError("multiprocessing-dag can only execute DAGs")
    scheduler = mpdag.Runner()
    pipeline = convert_graph(esrfgraph, varinfo)
    scheduler.run(pipeline)
